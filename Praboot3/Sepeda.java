
public class Sepeda implements PartsSepeda,PartsKekuatan{
	 String merek = "BaqyandCycle";
	 String dudukan = "Carbonate";
	 int typeban = 24;

	public void getMerek(){
		System.out.println("\n\n----Baqyand Cycle----");
		System.out.println("Sepeda ini bermerek : " + merek);
	}
	public void getDudukan(){
		System.out.println("dengan dudukan : " + dudukan);
	}
	public void getBan(){
		System.out.println("memiliki tipe ban : " + typeban);
	}
	public void digunakan(){
		System.out.println("sepeda ini kuat untuk di bawa santai");
	}
	public void perawatan(){
		System.out.println("untuk perawatan simpan di tempat yang aman saja");
	}

	public void hasil(){
		getMerek();
		getDudukan();
		digunakan();
		perawatan();
	}
}
